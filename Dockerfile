FROM cirrusci/flutter:stable

COPY . /build

WORKDIR /sdks/flutter
RUN git checkout 1.22.0


WORKDIR /build

RUN flutter doctor
RUN flutter clean
RUN flutter pub get