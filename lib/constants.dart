class Constants {
  static const String appName = 'Kforo';
  static const String logoTag = 'near.huscarl.loginsample.logo';
  static const String titleTag = 'near.huscarl.loginsample.title';
  static const String mainLogo = 'assets/images/kforo-logo.png';
  static const String userLogo = 'assets/images/user.png ';
  static const String logo = 'assets/images/ecorp.png';
}
