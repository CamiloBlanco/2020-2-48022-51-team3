import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kforo/models/building_model.dart';
import 'package:kforo/models/room_model.dart';
import 'package:kforo/src/widgets/forms/create_rooms.dart';
import 'package:kforo/src/widgets/layouts/form_dialog_layout.dart';
import 'package:kforo/src/widgets/layouts/list_layout.dart';

enum WhyFarther { harder, smarter, selfStarter, tradingCharter }

class ViewRooms extends StatefulWidget {
  final BuildingModel building;

  ViewRooms({this.building});

  @override
  _ViewRoomsState createState() => _ViewRoomsState();
}

class _ViewRoomsState extends State<ViewRooms> {
  bool _loading = true;
  bool _fetch = true;
  List<RoomModel> _rooms = [];

  Future<void> _fetchRooms({bool state: true}) async {
    final fetchedRooms = await widget.building.rooms();

    setState(() {
      _rooms = fetchedRooms;
      _loading = false;
      _fetch = false;
    });
  }

  AwesomeDialog _createRoomDialog({RoomModel room}) {
    final key = GlobalKey<CreateRoomsFormState>();

    return formDialogLayout(
        context: context,
        body: CreateRoomsForm(
          key,
          room: room,
          buildingReference: widget.building.reference,
        ),
        btnOkOnPress: () async {
          if (key.currentState.validate()) {
            bool saved = await key.currentState.send();
            return saved;
          }
          return null;
        },
        onSucess: () async {
          await widget.building.fetchRooms();

          setState(() {
            _fetch = true;
            _loading = true;
          });
          Navigator.of(context, rootNavigator: true).pop();
        },
        mainIcon: Icons.class_);
  }

  Widget _options(RoomModel room) {
    return Wrap(spacing: 3, children: [
      IconButton(
          icon: Icon(Icons.edit),
          onPressed: () => _createRoomDialog(room: room)),
    ]);
  }

  Widget _roomCard(RoomModel room) {
    return Card(
      child: Column(
        children: [
          ListTile(
            leading: Icon(
              Icons.class_,
              size: 35,
            ),
            title: Text(
              room.name,
              style: TextStyle(fontSize: 20),
            ),
            subtitle: Text(
              "Capacidad ${room.capacity}",
            ),
            trailing: _options(room),
          ),
        ],
      ),
    );
  }

  Widget _roomList() {
    return Container(
      child: Column(children: _rooms.map(_roomCard).toList()),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (_fetch) {
      _fetchRooms(state: false);
    }
    return ListLayout(
      title: 'KForo - Salones',
      child: _loading ? LinearProgressIndicator() : _roomList(),
      actionBtnPressed: _createRoomDialog,
    );
  }
}
