import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kforo/models/user_model.dart';
import 'package:kforo/src/widgets/layouts/list_layout.dart';
import 'package:kforo/src/widgets/screens/create_users.dart';

class UsersScreen extends StatefulWidget {
  @override
  UsersScreenState createState() => UsersScreenState();
}

class UsersScreenState extends State<UsersScreen> {
  List<UserModel> _users = [];
  bool _loading = true;

  Widget _userCard(UserModel user) {
    return Card(
        child: Column(children: [
      ListTile(
        leading: Icon(
          FontAwesomeIcons.user,
          size: 35,
        ),
        title: Text(
          user.name,
        ),
        subtitle: Text(
          "${user.role.name}",
          style: TextStyle(fontSize: 12),
        ),
        trailing: Wrap(spacing: 3, children: [
          IconButton(
              icon: Icon(Icons.edit),
              onPressed: () => _navigateToUserForm(user: user))
        ]),
      )
    ]));
  }

  Widget _userList() {
    return Container(
      child: Column(children: _users.map(_userCard).toList()),
    );
  }

  Future<void> fetchUsers({AsyncSnapshot<QuerySnapshot> snapshot}) async {
    List<UserModel> users;

    if (snapshot == null) {
      setState(() => _loading = true);
      users = await UserModel.listFromSnapshot(
          (await UserModel.collectionReference.get()).docs);
    } else {
      users = await UserModel.listFromSnapshot(snapshot.data.docs);
    }

    setState(() {
      _users = users;
      _loading = false;
    });
  }

  void _navigateToUserForm({UserModel user}) => Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => CreateUsers(
                onSuccess: fetchUsers,
                user: user,
              )));

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: UserModel.collectionReference.snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasData && _loading) {
            fetchUsers(snapshot: snapshot);
          }

          return ListLayout(
            title: 'KForo - Usuarios',
            child: _loading ? LinearProgressIndicator() : _userList(),
            actionBtnPressed: _navigateToUserForm,
          );
        });
  }
}
