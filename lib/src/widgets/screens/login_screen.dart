import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:flutter/material.dart';
import 'package:kforo/models/user_model.dart';
import 'package:kforo/src/flutter_login/flutter_login.dart';
import 'package:kforo/src/utils/regex_patterns.dart';
import 'dashboard_screen.dart';

import '../../../constants.dart';
import '../../../router.dart';
import '../../models/users.dart';

class LoginScreen extends StatelessWidget {
  static const routeName = '/auth';
  final List rol = [null];

  Duration get loginTime => Duration(milliseconds: timeDilation.ceil() * 2250);

  Future<String> _loginUser(LoginData data) {
    return Future.delayed(loginTime).then((_) async {
      try {
        await UserModel.signin(data.name, data.password);
        if (UserModel.currentUser != null) return null;
      } catch (exception) {}
      return "Email o contraseña incorrectos!";
    });
  }

  Future<String> _recoverPassword(String name) {
    return Future.delayed(loginTime).then((_) {
      if (!mockUsers.containsKey(name)) {
        return 'Username not exists';
      }
      return null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return FlutterLogin(
        title: Constants.appName,
        logo: Constants.mainLogo,
        logoTag: Constants.logoTag,
        titleTag: Constants.titleTag,
        emailValidator: (value) {
          if (!reEmail.hasMatch(value)) {
            return "Correo ingresado no válido";
          }
          return null;
        },
        passwordValidator: (value) {
          if (value.isEmpty) {
            return 'Contraseña esta vacia';
          }
          return null;
        },
        onLogin: (loginData) => _loginUser(loginData),
        onSignup: (loginData) => _loginUser(loginData),
        onSubmitAnimationCompleted: () {
          Navigator.of(context).pushReplacement(FadePageRoute(
            builder: (context) => DashboardScreen(),
          ));
        },
        onRecoverPassword: (name) => _recoverPassword(name));
  }
}
