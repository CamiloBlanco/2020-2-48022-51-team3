import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kforo/models/document_type_model.dart';
import 'package:kforo/models/role_model.dart';
import 'package:kforo/models/user_model.dart';
import 'package:kforo/src/flutter_login/src/widgets/animated_text_form_field.dart';
import 'package:kforo/src/utils/regex_patterns.dart';
import 'package:kforo/src/widgets/inputs/general_input.dart';
import 'package:kforo/src/widgets/forms/drop_down_form_field.dart';

class CreateUsers extends StatefulWidget {
  final UserModel user;
  final Function onSuccess;
  const CreateUsers({this.onSuccess, this.user, Key key}) : super(key: key);

  @override
  _CreateUsersState createState() => _CreateUsersState();
}

class _CreateUsersState extends State<CreateUsers> {
  final _formKey = new GlobalKey<FormState>();

  bool _loading = true,
      _loadingRoles = true,
      _loadingDocumentTypes = true,
      _sendingUserData = false,
      _emailAlreadyExist = false,
      _update = false;

  List<RoleModel> _roles;
  List<DocumentTypeModel> _documentTypes;

  String _documment, _name, _email, _password, _pass2;
  RoleModel _role;
  DocumentTypeModel _documentType;

  @override
  initState() {
    super.initState();
    if (widget.user != null) {
      _role = widget.user.role;
      _documentType = widget.user.documentType;
      _documment = widget.user.document;
      _name = widget.user.name;
      _email = widget.user.email;
      _update = true;
    }
  }

  bool _isLoading() => _loadingRoles || _loadingDocumentTypes;
  bool _formIsComplete() =>
      _role != null &&
      _documentType != null &&
      _documment != null &&
      _name != null &&
      ((_email != null && _password != null && _pass2 != null) || _update);

  Widget _createUserForm() {
    final inputs = <Widget>[];

    inputs.add(DropDownFormField(
      titleText: 'Tipo de usuario.',
      hintText: 'Selecciona una opción',
      value: _role,
      onSaved: (value) => setState(() => _role = value),
      onChanged: (value) => setState(() => _role = value),
      dataSource:
          _roles.map((role) => {"display": role.name, "value": role}).toList(),
      textField: 'display',
      valueField: 'value',
      validator: (_) =>
          _role == null ? "Seleccione un tipo de usuario, por favor." : null,
    ));

    inputs.add(DropDownFormField(
      titleText: 'Tipo de documento.',
      hintText: 'Selecciona una opción',
      value: _documentType,
      onSaved: (value) => setState(() => _documentType = value),
      onChanged: (value) => setState(() => _documentType = value),
      dataSource: _documentTypes
          .map((docType) => {"display": docType.name, "value": docType})
          .toList(),
      textField: 'display',
      valueField: 'value',
      validator: (_) => _documentType == null
          ? "Seleccione un tipo de documento, por favor."
          : null,
    ));

    if (_documentType != null)
      inputs.add(GeneralInputField(
        width: 100,
        keyboardType: TextInputType.visiblePassword,
        labelText: "Número de documento",
        autovalidateMode: AutovalidateMode.onUserInteraction,
        initialValue: _documment != null ? _documment : null,
        onChange: (value) {
          if (RegExp(_documentType.validationRegex).hasMatch(value)) {
            setState(() => _documment = value);
          } else {
            setState(() => _documment = null);
          }
        },
        validator: (value) {
          if (value.isEmpty)
            return "El documento(${_documentType.name}) no puede estar vacío.";
          if (!RegExp(_documentType.validationRegex).hasMatch(value))
            return _documentType.errorMsg;
          return null;
        },
      ));

    inputs.add(GeneralInputField(
      prefixIcon: Icon(Icons.person),
      width: 100,
      keyboardType: TextInputType.text,
      labelText: "Nombre completo",
      autovalidateMode: AutovalidateMode.onUserInteraction,
      initialValue: _name,
      onChange: (value) {
        if (reName.hasMatch(value)) {
          setState(() => _name = value);
        } else {
          setState(() => _name = null);
        }
      },
      validator: (value) {
        if (value.isEmpty) return "El nombre no puede estar vacío.";
        if (!reName.hasMatch(value))
          return "El campo tiene caracteres no permitidos.";
        return null;
      },
    ));

    if (_update) {
      inputs.add(GeneralInputField(
        prefixIcon: Icon(Icons.email),
        width: 100,
        keyboardType: TextInputType.emailAddress,
        labelText: "Correo electronico",
        initialValue: _email,
        enabled: false,
      ));
    } else {
      inputs.add(GeneralInputField(
        prefixIcon: Icon(Icons.email),
        width: 100,
        keyboardType: TextInputType.emailAddress,
        labelText: "Correo electronico",
        autovalidateMode: AutovalidateMode.onUserInteraction,
        initialValue: _email,
        onChange: (value) {
          if (_emailAlreadyExist) setState(() => _emailAlreadyExist = false);

          if (reEmail.hasMatch(value)) {
            setState(() => _email = value);
          } else {
            setState(() => _email = null);
          }
        },
        validator: (value) {
          if (_emailAlreadyExist) return "Este correo ya está siendo usado.";
          if (value.isEmpty) return "El email no puede estar vacío.";
          if (!reEmail.hasMatch(value))
            return "El emial no tiene un formato válido.";
          return null;
        },
      ));
      inputs.add(Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black),
          borderRadius: const BorderRadius.all(
            const Radius.circular(100.0),
          ),
        ),
        padding: EdgeInsets.only(right: 16, left: 16, top: 0),
        child: AnimatedPasswordTextFormField(
          animatedWidth: 100,
          labelText: 'Contraseña',
          textInputAction: TextInputAction.next,
          onChange: (value) {
            if (rePassword.hasMatch(value)) {
              setState(() => _password = value);
            } else {
              setState(() => _password = null);
            }
          },
          validator: (value) {
            if (value.isEmpty) return "La contraseña no puede estar vacía.";
            if (!rePassword.hasMatch(value))
              return "La contraseña debe ser alfanumérica más (?!=.-_) y tener entre 8 y 20 caracteres.";
            return null;
          },
        ),
      ));

      inputs.add(Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black),
          borderRadius: const BorderRadius.all(
            const Radius.circular(100.0),
          ),
        ),
        padding: EdgeInsets.only(right: 16, left: 16, top: 0),
        child: AnimatedPasswordTextFormField(
          animatedWidth: 100,
          labelText: 'Repetir contraseña',
          textInputAction: TextInputAction.next,
          onChange: (value) {
            setState(() => _pass2 = value);
          },
          validator: (value) {
            if (value.isEmpty) return "Por favor ingrese el campo.";
            if (_pass2 != _password) return "Las contraseñas no coinciden.";
            return null;
          },
        ),
      ));
    }

    if (_formIsComplete()) {
      inputs.add(RaisedButton(
        onPressed: () async {
          if (_formKey.currentState.validate()) {
            // formKey.currentState.save();
            setState(() => _sendingUserData = true);

            try {
              if (_update) {
                await widget.user.update(
                    name: _name,
                    document: _documment,
                    documentType: _documentType,
                    role: _role);
              } else {
                await UserModel.signUp(
                    name: _name,
                    document: _documment,
                    documentType: _documentType.reference,
                    email: _email,
                    password: _password,
                    role: _role.reference);
              }

              await widget.onSuccess();
              setState(() => _sendingUserData = false);

              Navigator.pop(context);
            } catch (error) {
              switch (error.code) {
                case "email-already-in-use":
                  setState(() {
                    _emailAlreadyExist = true;
                    _sendingUserData = false;
                  });
                  _showEmailAlreadyExistMessage();
                  break;
                default:
                  setState(() {
                    _sendingUserData = false;
                  });
                  _showSubmitUnknownErrorMessage();
              }
            }
          }
        },
        child: Text(
          'Guardar',
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
        color: Colors.deepPurple,
        splashColor: Colors.white30,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
            side: BorderSide(color: Colors.grey[300])),
      ));
    }

    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: inputs
            .map((input) => Container(
                  padding: EdgeInsets.all(16),
                  child: input,
                ))
            .toList(),
      ),
    );
  }

  Future<void> _fetchRoles() async {
    final roles = await RoleModel.fetchAll();

    setState(() {
      _roles = roles;
      _loadingRoles = false;
      _loading = _isLoading();
    });
  }

  Future<void> _fetchDocumentTypes() async {
    final documentTypes = await DocumentTypeModel.fetchAll();

    setState(() {
      _documentTypes = documentTypes;
      _loadingDocumentTypes = false;
      _loading = _isLoading();
    });
  }

  _showSubmitUnknownErrorMessage() {
    showDialog(
        context: context,
        builder: (_) => new AlertDialog(
            title: Text("Error inesperado :("),
            content: Text(
                "Lo sentimos intentelo más tarde. Sí el error persiste contacte a soporte.")));
  }

  _showEmailAlreadyExistMessage() {
    showDialog(
        context: context,
        builder: (_) => new AlertDialog(
            title: Text("Error de creación"),
            content:
                Text("Lo sentimos el correo $_email ya se encuentra en uso.")));
  }

  @override
  Widget build(BuildContext context) {
    if (_loadingRoles) _fetchRoles();
    if (_loadingDocumentTypes) _fetchDocumentTypes();

    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('KForo - Crear Usuario'),
        ),
        body: Padding(
          padding: EdgeInsets.all(16.0),
          child: SingleChildScrollView(
              child: _loading || _sendingUserData
                  ? LinearProgressIndicator()
                  : _createUserForm()),
        ),
      ),
    );
  }
}
