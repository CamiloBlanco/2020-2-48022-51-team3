import 'package:flutter/material.dart';
import 'package:kforo/src/widgets/buttons/progress_button.dart';
import 'package:kforo/src/widgets/inputs/dropdown.dart';
import 'package:kforo/src/widgets/inputs/text_field.dart';

class UserRegistrationScreen extends StatefulWidget {
  static const routeName = '/userregistration';

  @override
  _UserRegistrationState createState() => _UserRegistrationState();
}

class _UserRegistrationState extends State<UserRegistrationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Registro de Usuarios"),
          centerTitle: true,
        ),
        body: Center(
          child: ListView(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(20),
                child: CircleAvatar(
                  radius: 80.0,
                  backgroundColor: Colors.black,
                  child: CircleAvatar(
                      radius: 70.0,
                      backgroundColor: Colors.white,
                      foregroundColor: Colors.black,
                      child: Icon(
                        Icons.person_add,
                        size: 100,
                      )),
                ),
              ),
              CustomDropdown(text: "Rol"),
              BeautyTextfield(
                width: double.maxFinite,
                height: 60,
                prefixIcon: Icon(Icons.account_circle),
                inputType: TextInputType.text,
                duration: Duration(milliseconds: 300),
                placeholder: "Nombre",
                onTap: () {
                  print('Click');
                },
                onChanged: (text) {
                  print(text);
                },
                onSubmitted: (data) {
                  print(data.length);
                },
              ),
              BeautyTextfield(
                width: double.maxFinite,
                height: 60,
                duration: Duration(milliseconds: 300),
                inputType: TextInputType.text,
                prefixIcon: Icon(Icons.account_box),
                placeholder: "Apellidos",
                onTap: () {
                  print('Click');
                },
                onChanged: (text) {
                  print(text);
                },
                onSubmitted: (data) {
                  print(data.length);
                },
              ),
              BeautyTextfield(
                width: double.maxFinite,
                height: 60,
                duration: Duration(milliseconds: 300),
                inputType: TextInputType.text,
                prefixIcon: Icon(Icons.contacts),
                placeholder: "Documento",
                onTap: () {
                  print('Click');
                },
                onChanged: (text) {
                  print(text);
                },
                onSubmitted: (data) {
                  print(data.length);
                },
              ),
              CustomProgressButton(
                color: Colors.deepPurple,
                icon: Icons.send,
                text: "Enviar",
                onPressed: () => {},
                onSucess: () => {},
              )
            ],
          ),
        ));
  }
}
