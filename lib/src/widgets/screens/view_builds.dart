import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kforo/models/building_model.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kforo/src/widgets/forms/create_builds.dart';
import 'package:kforo/src/widgets/layouts/form_dialog_layout.dart';
import 'package:kforo/src/widgets/layouts/list_layout.dart';
import 'package:kforo/src/widgets/screens/view_rooms.dart';

class ViewBuilds extends StatefulWidget {
  @override
  _ViewBuildsState createState() => _ViewBuildsState();
}

class _ViewBuildsState extends State<ViewBuilds> {
  List<BuildingModel> _buildings = [];
  bool _loading = true;

  Future<void> _refreshBuildings() async {
    setState(() => _loading = true);
    BuildingModel.collectionReference.get().then((snap) {
      final freshBuildings = BuildingModel.listFromSnapshot(snap.docs);

      setState(() {
        _buildings = freshBuildings;
        _loading = false;
      });
    });
  }

  AwesomeDialog _buildingFormDialog({BuildingModel building}) {
    GlobalKey<CreateBuildFormState> key = GlobalKey<CreateBuildFormState>();

    return formDialogLayout(
        context: context,
        body: CreateBuildForm(key, building: building),
        btnOkOnPress: () async {
          if (key.currentState.validate()) {
            bool saved = await key.currentState.send();
            return saved;
          }
          return null;
        },
        onSucess: () {
          if (building == null) {
            _refreshBuildings();
          }
          Navigator.of(context, rootNavigator: true).pop();
        },
        mainIcon: Icons.business,
        okBtnIcon: building == null ? null : Icons.save);
  }

  Widget _buildCard(BuildingModel building) {
    return Card(
        child: Column(children: [
      ListTile(
        leading: Icon(
          FontAwesomeIcons.building,
          size: 35,
        ),
        title: Text(
          building.name,
        ),
        subtitle: Text(
          "Capacidad ${building.capacity}",
        ),
        trailing: Wrap(spacing: 3, children: [
          IconButton(
              icon: Icon(Icons.edit),
              onPressed: () => _buildingFormDialog(building: building)),
          IconButton(
              icon: Icon(Icons.collections_bookmark),
              onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ViewRooms(building: building))))
        ]),
      )
    ]));
  }

  Widget _buildingList() {
    return Container(
      child: Column(children: _buildings.map(_buildCard).toList()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: BuildingModel.collectionReference.snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          _buildings = BuildingModel.listFromSnapshot(snapshot.data.docs);
          _loading = false;
        }

        return ListLayout(
          title: 'KForo - Edificios',
          child: _loading ? LinearProgressIndicator() : _buildingList(),
          actionBtnPressed: _buildingFormDialog,
        );
      },
    );
  }
}
