import 'package:flutter/material.dart';
import 'package:kforo/models/document_type_model.dart';
import 'package:kforo/src/widgets/forms/drop_down_form_field.dart';
import 'package:kforo/src/widgets/inputs/general_input.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

class DialUsersScreen extends StatefulWidget {
  @override
  _DialUsersScreen createState() => _DialUsersScreen();
}

class _DialUsersScreen extends State<DialUsersScreen> {
  bool _loadingDocumentTypes = true;
  bool _loading = true;
  TextEditingController _documentController = new TextEditingController();
  TextEditingController _temperatureController = new TextEditingController();
  String documentValue;
  String temperatureValue;

  String valueDocumento = "";
  String valueGrupo = "";
  String valueSalon = "";
  int selectedRadioGroup;
  int selectRadioTileGroup;
  int selectedRadioRoom;
  int selectedRadioTileRoom;

  bool haveKit = false;
  String _scanBarcode = 'Unknown';

  List<DocumentTypeModel> _documentTypes;
  DocumentTypeModel _documentType;

  bool _isLoading() => _loadingDocumentTypes;

  @override
  void initState() {
    super.initState();
    selectedRadioGroup = 0;
    selectRadioTileGroup = 0;
    selectedRadioRoom = 1;
    selectedRadioTileRoom = 1;
  }

  setSelectedRadioGroup(int valGroup) {
    setState(() {
      selectedRadioGroup = valGroup;
    });
  }

  setSelectedRadioTileGroup(int valGroup) {
    setState(() {
      selectRadioTileGroup = valGroup;
    });
  }

  setSelectedRadioRoom(int valRoom) {
    setState(() {
      selectedRadioRoom = valRoom;
    });
  }

  setSelectedRadioTileRoom(int valRoom) {
    print(valRoom);
    setState(() {
      selectedRadioTileRoom = valRoom;
    });
  }

  startBarcodeScanStream() async {
    FlutterBarcodeScanner.getBarcodeStreamReceiver(
            "#ff6666", "Cancel", true, ScanMode.BARCODE)
        .listen((barcode) => print(barcode));
  }

  Future<void> scanBarcodeNormal() async {
    String barcodeScanRes;
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          "#ff6666", "Cancel", true, ScanMode.BARCODE);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    if (!mounted) return;
    print(barcodeScanRes);

    setState(() {
      _scanBarcode = barcodeScanRes;
      _documentController.text =
          int.parse(_scanBarcode.substring(48, 58)).toString();
      print('resultado: ' + _scanBarcode);
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_loadingDocumentTypes) _fetchDocumentTypes();

    return Scaffold(
        appBar: AppBar(
          title: Text("Ingresar Usuarios"),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(
                            left: 45.0,
                            right: 45.0,
                            bottom: 10,
                            top: 15,
                          ),
                          child: _loading
                              ? LinearProgressIndicator()
                              : DropDownFormField(
                                  titleText: "Tipo de documento",
                                  hintText: "Seleccione una opción",
                                  value: _documentType,
                                  onSaved: (value) =>
                                      setState(() => _documentType = value),
                                  onChanged: (value) =>
                                      setState(() => _documentType = value),
                                  dataSource: _documentTypes
                                      .map((docType) => {
                                            "display": docType.name,
                                            "value": docType
                                          })
                                      .toList(),
                                  textField: 'display',
                                  valueField: 'value',
                                  validator: (_) => _documentType == null
                                      ? "Seleccione un tipo de documento"
                                      : null,
                                ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 45.0, right: 45.0, bottom: 10),
                          child: GeneralInputField(
                            controller: _documentController,
                            onChange: (value) =>
                                setState(() => documentValue = value),
                            labelText: "Documento $valueDocumento",
                            prefixIcon: Icon(
                              MdiIcons.cardAccountDetails,
                              size: 20,
                            ),
                            width: 10,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 45.0, right: 45.0, bottom: 10),
                          child: GeneralInputField(
                            controller: _temperatureController,
                            onChange: (value) =>
                                setState(() => temperatureValue = value),
                            labelText: "Temperatura",
                            keyboardType: TextInputType.number,
                            prefixIcon: Icon(
                              MdiIcons.temperatureCelsius,
                              size: 20,
                            ),
                            width: 10,
                          ),
                        ),
                        Center(
                            child: Padding(
                                padding: EdgeInsets.only(
                                    left: 70, top: 10, bottom: 10),
                                child: Row(children: <Widget>[
                                  Checkbox(
                                    activeColor: Colors.deepPurple,
                                    value: haveKit,
                                    onChanged: (bool value) {
                                      setState(() {
                                        haveKit = value;
                                      });
                                    },
                                  ),
                                  Text("¿Tiene kit de desinfección?"),
                                ])))
                      ],
                    ),
                  ),
                  secondCard(_documentType, _documentController,
                      _temperatureController),
                  thirdCard(),
                ],
              ),
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
            highlightElevation: 0,
            backgroundColor: Colors.deepPurple,
            child: Icon(
              Icons.qr_code_sharp,
              color: Colors.white,
            ),
            onPressed: () {
              scanBarcodeNormal();
            }));
  }

  Future<void> _fetchDocumentTypes() async {
    final documentTypes = await DocumentTypeModel.fetchAll();

    setState(() {
      _documentTypes = documentTypes;
      _loadingDocumentTypes = false;
      _loading = _isLoading();
    });
  }

  Widget secondCard(
      DocumentTypeModel document,
      TextEditingController documentController,
      TextEditingController temperatureController) {
    if (document != null &&
        documentController.text != '' &&
        temperatureController.text != '') {
      return Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ListTile(
              title: Text("Grupos"),
            ),
            Padding(
              padding:
                  EdgeInsets.only(left: 45.0, right: 45.0, bottom: 10, top: 10),
              child: DropDownFormField(
                titleText: "Grupos",
                hintText: "Seleccione un grupo",
                value: valueGrupo,
                dataSource: [
                  {
                    "display": "Grupo 1",
                    "value": "Grupo 1",
                  },
                  {
                    "display": "Grupo 2",
                    "value": "Grupo 2",
                  },
                  {
                    "display": "Grupo 3",
                    "value": "Grupo 3",
                  }
                ],
                onChanged: (value) => setState(() => valueGrupo = value),
                textField: "display",
                valueField: "value",
              ),
            ),
          ],
        ),
      );
    }
    return SizedBox(
      height: 0,
    );
  }

  Widget thirdCard() {
    if (valueGrupo != "") {
      return Card(
        child: Column(
          children: [
            ListTile(
              title: Text("Salones del $valueGrupo"),
            ),
            Padding(
              padding:
                  EdgeInsets.only(left: 45.0, right: 45.0, bottom: 10, top: 10),
              child: DropDownFormField(
                titleText: "Salones",
                hintText: "Seleccione un salón",
                value: valueSalon,
                dataSource: [
                  {
                    "display": "Salon 1",
                    "value": "Salon 1",
                  },
                  {
                    "display": "Salon 2",
                    "value": "Salon 2",
                  }
                ],
                onChanged: (value) => setState(() => valueSalon = value),
                textField: "display",
                valueField: "value",
              ),
            ),
            SizedBox(
              height: 20,
            ),
            ButtonTheme(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              minWidth: 150.0,
              height: 45.0,
              child: RaisedButton(
                onPressed: () {},
                child: Text(
                  "Guardar Ingreso",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      );
    }
    return SizedBox(
      height: 0,
    );
  }
}
