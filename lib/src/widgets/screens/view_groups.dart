import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ViewGroups extends StatefulWidget {
  @override
  _ViewGroupsState createState() => _ViewGroupsState();
}

class Groups{
  int id;
  String name;
  String capacidad;

  Groups(int id, String name, String capacidad){
    this.id = id; 
    this.name = name; 
    this.capacidad = capacidad;
  }
}

class _ViewGroupsState extends State<ViewGroups> {
  var groups1 = Groups(1, 'Psicologia', 'Capacidad 610');
  var groups2 = Groups(2, 'Matemáticas', 'Capacidad 60');
  var groups3 = Groups(3, 'Sistemas', 'Capacidad 100');

  void descriptionGroups(){
    showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog (
          title: Text(
            'Descripción',
            textAlign: TextAlign.center
          ),
          content: Text (
            'Esto es una descripción de los grupos que se seleccionan',
            textAlign: TextAlign.center
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Salir'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  Widget groupsCard(groups) {
    return Card(
        child: Column(children: [
      ListTile(
        leading: Icon(
          FontAwesomeIcons.users,
          size: 35,
        ),
        title: Text(
          groups.name,
        ),
        subtitle: Text(
          groups.capacidad
        ),
        trailing: Wrap(spacing: 3, children: [
          IconButton(
            icon: Icon(
              FontAwesomeIcons.eye,
            ),
            onPressed: () => descriptionGroups()
          ),
          IconButton(
            icon: Icon(Icons.edit),
            onPressed: () {
                print('editar salon');
            }
          ),
        ]),
      )
    ]));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('KForo - Grupos'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Container(
            child: Column(children: [
              groupsCard(groups1),
              groupsCard(groups2),
              groupsCard(groups3),
            ]),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            print('Entre');
          },
          child: Icon(Icons.add),
          backgroundColor: Colors.purple[800],
        ),
      ),
    );
  }
}