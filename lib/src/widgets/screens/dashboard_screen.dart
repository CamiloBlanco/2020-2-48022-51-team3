import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kforo/models/role_model.dart';
import 'package:kforo/models/user_model.dart';
import 'package:kforo/src/flutter_login/src/widgets/hero_text.dart';
import 'package:kforo/src/widgets/animations/fade_in.dart';
import 'package:kforo/src/widgets/buttons/round_button.dart';
import 'package:kforo/src/widgets/screens/users_screen.dart';
import 'package:kforo/src/widgets/screens/view_groups.dart';
import '../../../transition_route_observer.dart';
import '../../../constants.dart';
import 'package:kforo/src/widgets/screens/dial_users.dart';

import 'view_builds.dart';

class DashboardScreen extends StatefulWidget {
  static const routeName = '/dashboard';

  DashboardScreen();

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen>
    with SingleTickerProviderStateMixin, TransitionRouteAware {
  Future<bool> _goToLogin(BuildContext context) async {
    await UserModel.currentUser.signOut();

    return Navigator.of(context)
        .pushReplacementNamed('/')
        // we dont want to pop the screen, just replace it completely
        .then((_) => false);
  }

  final routeObserver = TransitionRouteObserver<PageRoute>();

  static const headerAniInterval =
      const Interval(.1, .3, curve: Curves.easeOut);
  AnimationController _loadingController;

  Widget _buildButton(
      {Widget icon,
      String label,
      Interval interval,
      Function event,
      Color backgroundColor}) {
    return RoundButton(
      icon: icon,
      label: label,
      loadingController: _loadingController,
      interval: Interval(
        interval.begin,
        interval.end,
        curve: ElasticOutCurve(0.42),
      ),
      backgroundColor: backgroundColor,
      onPressed: event,
    );
  }

  List<Widget> _getButtonsByRol(RoleModel role) {
    const step = 0.04;
    const aniInterval = 0.75;

    if (role.isAdmin) {
      return [
        _buildButton(
          icon: Icon(FontAwesomeIcons.building),
          label: 'Edificios',
          interval: Interval(0, aniInterval),
          event: () => Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ViewBuilds()),
          ),
        ),
        _buildButton(
            icon: Icon(FontAwesomeIcons.user),
            label: 'Usuarios',
            interval: Interval(0, aniInterval),
            event: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => UsersScreen()),
                )),
        
        _buildButton(
          icon: Icon(FontAwesomeIcons.users),
          label: 'Grupos',
          interval: Interval(0, aniInterval),
          event: () => Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ViewGroups()),
          ),
        )
      ];
    } else if (role.isAWatchman) {
      return [
        _buildButton(
          icon: Icon(Icons.directions_run, size: 20),
          label: 'Ingreso',
          interval: Interval(step, aniInterval + step),
          event: () => Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => DialUsersScreen()),
          ),
        )
      ];
    } else if (role.isPerson) {
      return [
        _buildButton(
          icon: Icon(FontAwesomeIcons.chartLine),
          label: 'Report',
          interval: Interval(0, aniInterval),
        ),
        _buildButton(
          icon: Icon(Icons.vpn_key),
          label: 'Register',
          interval: Interval(step, aniInterval + step),
        ),
        _buildButton(
          icon: Icon(FontAwesomeIcons.history),
          label: 'History',
          interval: Interval(step * 2, aniInterval + step * 2),
        ),
        _buildButton(
          icon: Icon(FontAwesomeIcons.ellipsisH),
          label: 'Other',
          interval: Interval(0, aniInterval),
        )
      ];
    }

    return [];
  }

  @override
  void initState() {
    super.initState();

    _loadingController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1250),
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context));
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    _loadingController.dispose();
    super.dispose();
  }

  @override
  void didPushAfterTransition() => _loadingController.forward();

  AppBar _buildAppBar(ThemeData theme) {
    final signOutBtn = IconButton(
      icon: const Icon(FontAwesomeIcons.signOutAlt),
      color: theme.accentColor,
      onPressed: () => _goToLogin(context),
    );
    final title = Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: 50),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Hero(
              tag: Constants.logoTag,
              child: Image.asset(
                Constants.mainLogo,
                filterQuality: FilterQuality.high,
                height: 30,
              ),
            ),
          ),
          HeroText(
            Constants.appName,
            tag: Constants.titleTag,
            viewState: ViewState.shrunk,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              color: Colors.orange,
            ),
          ),
          SizedBox(width: 20),
        ],
      ),
    );

    return AppBar(
      actions: <Widget>[
        FadeIn(
          child: signOutBtn,
          controller: _loadingController,
          offset: .3,
          curve: headerAniInterval,
          fadeDirection: FadeDirection.endToStart,
        ),
      ],
      title: title,
      backgroundColor: theme.primaryColor.withOpacity(.1),
      elevation: 0,
      textTheme: theme.accentTextTheme,
      iconTheme: theme.accentIconTheme,
    );
  }

  Widget _buildDashboardGrid() {
    return GridView.count(
        padding: const EdgeInsets.symmetric(
          horizontal: 32.0,
          vertical: 20,
        ),
        childAspectRatio: .9,
        crossAxisCount: 3,
        children: _getButtonsByRol(UserModel.currentUser.role));
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return WillPopScope(
      onWillPop: () => _goToLogin(context),
      child: SafeArea(
        child: Scaffold(
          appBar: _buildAppBar(theme),
          body: Container(
            width: double.infinity,
            height: double.infinity,
            color: theme.primaryColor.withOpacity(.1),
            child: Stack(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    SizedBox(height: 80),
                    Expanded(
                      flex: 8,
                      child: ShaderMask(
                        shaderCallback: (Rect bounds) {
                          return LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            tileMode: TileMode.clamp,
                            colors: <Color>[
                              Colors.deepPurpleAccent.shade100,
                              Colors.deepPurple.shade100,
                              Colors.deepPurple.shade100,
                              Colors.deepPurple.shade100,
                            ],
                          ).createShader(bounds);
                        },
                        child: _buildDashboardGrid(),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
