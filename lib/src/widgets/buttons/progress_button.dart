import 'dart:async';

import 'package:flutter/material.dart';
import 'package:progress_state_button/iconed_button.dart';
import 'package:progress_state_button/progress_button.dart';

class CustomProgressButton extends StatefulWidget {
  CustomProgressButton({
    Key key,
    this.title,
    this.text,
    @required this.color,
    @required this.onPressed,
    @required this.onSucess,
    this.onFailure,
    @required this.icon,
    this.delayAfterSucess = 1500,
    this.delayAfterFailure = 1500,
  }) : super(key: key);

  final String title;
  final Color color;
  final String text;
  final IconData icon;
  final Function onPressed;
  final Function onSucess;
  final Function onFailure;
  final int delayAfterSucess;
  final int delayAfterFailure;

  @override
  _ProgressButtonState createState() => _ProgressButtonState();
}

class _ProgressButtonState extends State<CustomProgressButton> {
  ButtonState stateOnlyText = ButtonState.idle;
  ButtonState stateTextWithIcon = ButtonState.idle;

  Widget buildTextWithIcon() {
    return ProgressButton.icon(
        iconedButtons: {
          ButtonState.idle: IconedButton(
              text: widget.text,
              icon: Icon(widget.icon, color: Colors.white),
              color: widget.color),
          ButtonState.loading:
              IconedButton(text: "Loading", color: widget.color),
          ButtonState.fail: IconedButton(
              text: "Falló",
              icon: Icon(Icons.cancel, color: Colors.white),
              color: Colors.red.shade300),
          ButtonState.success: IconedButton(
              text: "Listo",
              icon: Icon(
                Icons.check_circle,
                color: Colors.white,
              ),
              color: Colors.green.shade400)
        },
        onPressed: onPressedIconWithText(
            widget.onPressed, widget.onSucess, widget.onFailure),
        state: stateTextWithIcon);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          buildTextWithIcon(),
        ],
      ),
    );
  }

  Function onPressedIconWithText(
          Function onPressed, Function onSuccess, Function onFailure) =>
      () async {
        switch (stateTextWithIcon) {
          case ButtonState.idle:
            stateTextWithIcon = ButtonState.loading;
            onPressed().then((done) async {
              if (done == null) {
                setState(() => stateTextWithIcon = ButtonState.idle);
              } else if (done) {
                setState(() => stateTextWithIcon = ButtonState.success);
                await Future.delayed(
                    Duration(milliseconds: widget.delayAfterSucess));
                onSuccess();
              } else {
                setState(() => stateTextWithIcon = ButtonState.fail);

                if (onFailure != null) {
                  await Future.delayed(
                      Duration(milliseconds: widget.delayAfterFailure));
                  onFailure();
                }

                Future.delayed(Duration(milliseconds: 1490)).then((_) =>
                    setState(() => stateTextWithIcon = ButtonState.idle));
              }
            }).catchError((e) async {
              setState(() => stateTextWithIcon = ButtonState.fail);

              if (onFailure != null) {
                await Future.delayed(
                    Duration(milliseconds: widget.delayAfterFailure));
                onFailure();
              }

              Future.delayed(Duration(milliseconds: 1490)).then(
                  (_) => setState(() => stateTextWithIcon = ButtonState.idle));
            });
            break;
          case ButtonState.loading:
          case ButtonState.success:
          case ButtonState.fail:
            break;
        }

        setState(() => stateTextWithIcon = stateTextWithIcon);
      };
}
