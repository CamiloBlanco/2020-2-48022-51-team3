import 'package:flutter/material.dart';

class QRButton extends StatefulWidget {
  @override
  _QRButtonState createState() => _QRButtonState();
}

class _QRButtonState extends State<QRButton> {
  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Ink(
        decoration: const ShapeDecoration(
            color: Colors.deepPurple, shape: CircleBorder()),
        child: IconButton(
          icon: Icon(Icons.qr_code_sharp),
          color: Colors.white,
          onPressed: () {},
        ),
      ),
      Text('Lectura QR'),
    ]);
  }
}
