import 'package:flutter/material.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:kforo/src/widgets/buttons/progress_button.dart';

AwesomeDialog formDialogLayout(
    {@required context,
    @required Widget body,
    @required Function btnOkOnPress,
    @required Function onSucess,
    @required IconData mainIcon,
    Function onFailure,
    String okBtnTxt = "Crear",
    IconData okBtnIcon,
    CustomProgressButton btnCancel,
    
    VoidCallback btnCancelOnPress}) {
  return AwesomeDialog(
    context: context,
    width: 500,
    headerAnimationLoop: false,
    customHeader: CircleAvatar(
      radius: 50,
      backgroundColor: Colors.deepPurple,
      child: CircleAvatar(
        radius: 45,
        backgroundColor: Colors.white,
        child: Icon(Icons.class_),
      ),
    ),
    animType: AnimType.BOTTOMSLIDE,
    title: 'INFO',
    desc: 'Dialog description here...',
    body: body,
    btnOk: CustomProgressButton(
      text: "",
      color: Colors.deepPurple,
      icon: okBtnIcon != null ? okBtnIcon : Icons.send,
      onSucess: onSucess,
      onPressed: btnOkOnPress,
      onFailure: onFailure,
    ),
    btnCancel: btnCancel,
    btnCancelOnPress: btnCancelOnPress,
  )..show();
}
