import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListLayout extends StatelessWidget {
  final Widget child;
  final String title;
  final Function actionBtnPressed;
  final bool scroll;

  ListLayout(
      {this.child, this.title, this.actionBtnPressed, this.scroll = true});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text(this.title),
        ),
        body: Padding(
            padding: const EdgeInsets.all(20.0),
            child:
                scroll ? SingleChildScrollView(child: this.child) : this.child),
        floatingActionButton: FloatingActionButton(
          onPressed: this.actionBtnPressed,
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
          backgroundColor: Colors.deepPurple,
        ),
      ),
    );
  }
}
