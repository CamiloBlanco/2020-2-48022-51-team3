import "package:flutter/material.dart";

class InputButton extends StatefulWidget {
  @override
  _InputButtonState createState() => _InputButtonState();
}

class _InputButtonState extends State<InputButton> {
  @override
  build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Ink(
        decoration: const ShapeDecoration(
          color: Colors.deepPurple,
          shape: CircleBorder(),
        ),
        child: IconButton(
          icon: Icon(Icons.app_registration),
          color: Colors.white,
          onPressed: () {},
        ),
      ),
      Text("Manualmente")
    ]);
  }
}
