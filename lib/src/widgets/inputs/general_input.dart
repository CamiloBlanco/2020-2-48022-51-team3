import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

enum TextFieldInertiaDirection {
  left,
  right,
}

class GeneralInputField extends StatefulWidget {
  GeneralInputField(
      {Key key,
      this.interval = const Interval(0.0, 1.0),
      @required this.width,
      this.loadingController,
      this.inertiaController,
      this.inertiaDirection,
      this.enabled = true,
      this.labelText,
      this.prefixIcon,
      this.suffixIcon,
      this.keyboardType,
      this.textInputAction,
      this.obscureText = false,
      this.controller,
      this.focusNode,
      this.validator,
      this.onFieldSubmitted,
      this.onSaved,
      this.autovalidateMode,
      this.initialValue,
      this.onChange})
      : assert((inertiaController == null && inertiaDirection == null) ||
            (inertiaController != null && inertiaDirection != null)),
        super(key: key);

  final Interval interval;
  final AnimationController loadingController;
  final AnimationController inertiaController;
  final double width;
  final bool enabled;
  final String labelText;
  final Widget prefixIcon;
  final Widget suffixIcon;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final bool obscureText;
  final TextEditingController controller;
  final FocusNode focusNode;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onFieldSubmitted;
  final FormFieldSetter<String> onSaved;
  final TextFieldInertiaDirection inertiaDirection;
  final AutovalidateMode autovalidateMode;
  final String initialValue;
  final Function(String) onChange;
  @override
  _GeneralTextInputField createState() => _GeneralTextInputField();
}

class _GeneralTextInputField extends State<GeneralInputField> {
  Animation<double> scaleAnimation;
  Animation<double> sizeAnimation;
  Animation<double> suffixIconOpacityAnimation;

  Animation<double> fieldTranslateAnimation;
  Animation<double> iconRotationAnimation;
  Animation<double> iconTranslateAnimation;

  Widget _buildInertiaAnimation(Widget child) {
    if (widget.inertiaController == null) {
      return child;
    }

    return AnimatedBuilder(
      animation: iconTranslateAnimation,
      builder: (context, child) => Transform(
        alignment: Alignment.center,
        transform: Matrix4.identity()
          ..translate(iconTranslateAnimation.value)
          ..rotateZ(iconRotationAnimation.value),
        child: child,
      ),
      child: child,
    );
  }

  InputDecoration _getInputDecoration(ThemeData theme) {
    return InputDecoration(
      filled: true,
      border: new OutlineInputBorder(
        borderRadius: const BorderRadius.all(
          const Radius.circular(100.0),
        ),
      ),
      labelText: widget.labelText,
      prefixIcon: _buildInertiaAnimation(widget.prefixIcon),
      suffixIcon: _buildInertiaAnimation(widget.loadingController != null
          ? FadeTransition(
              opacity: suffixIconOpacityAnimation,
              child: widget.suffixIcon,
            )
          : widget.suffixIcon),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    Widget textField = TextFormField(
      controller: widget.controller,
      focusNode: widget.focusNode,
      decoration: _getInputDecoration(theme),
      keyboardType: widget.keyboardType,
      textInputAction: widget.textInputAction,
      obscureText: widget.obscureText,
      onFieldSubmitted: widget.onFieldSubmitted,
      onSaved: widget.onSaved,
      validator: widget.validator,
      enabled: widget.enabled,
      autovalidateMode: widget.autovalidateMode,
      initialValue: widget.initialValue,
      onChanged: widget.onChange,
    );
    if (widget.loadingController != null) {
      textField = ScaleTransition(
        scale: scaleAnimation,
        child: AnimatedBuilder(
          animation: sizeAnimation,
          builder: (context, child) => ConstrainedBox(
            constraints: BoxConstraints.tightFor(width: sizeAnimation.value),
            child: child,
          ),
          child: textField,
        ),
      );
    }

    if (widget.inertiaController != null) {
      textField = AnimatedBuilder(
        animation: fieldTranslateAnimation,
        builder: (context, child) => Transform.translate(
          offset: Offset(fieldTranslateAnimation.value, 0),
          child: child,
        ),
        child: textField,
      );
    }
    return textField;
  }
}
