import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kforo/models/room_model.dart';
import 'package:kforo/src/widgets/inputs/general_input.dart';

class CreateRoomsForm extends StatefulWidget {
  final RoomModel room;
  final DocumentReference buildingReference;

  CreateRoomsForm(Key key, {this.room, @required this.buildingReference})
      : super(key: key);

  @override
  CreateRoomsFormState createState() => CreateRoomsFormState(room);
}

class CreateRoomsFormState extends State<CreateRoomsForm> {
  final _formKey = GlobalKey<FormState>();

  RoomModel _room;
  bool _updateMode;

  CreateRoomsFormState(this._room);

  @override
  void initState() {
    super.initState();
    if (this._room == null) {
      this._room = RoomModel(
          capacity: 0,
          location: '',
          name: '',
          buildingReference: widget.buildingReference);
      _updateMode = false;
    } else {
      _updateMode = true;
    }
  }

  Future<bool> send() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      if (_updateMode) {
        await this._room.update();
      } else {
        await this._room.createNew();
      }

      return true;
    } else {
      return false;
    }
  }

  bool validate() {
    return _formKey.currentState.validate();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Positioned(
              child: FittedBox(
                child: Card(
                  child: Container(
                    padding: const EdgeInsets.only(
                      left: 20,
                      top: 50,
                      right: 20,
                      bottom: 10,
                    ),
                    width: 300,
                    alignment: Alignment.center,
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          GeneralInputField(
                              width: 150,
                              prefixIcon: Icon(Icons.class_),
                              keyboardType: TextInputType.text,
                              labelText: "Nombre del salón",
                              initialValue: this._room.name.isEmpty
                                  ? null
                                  : this._room.name,
                              validator: (String value) =>
                                  value.trim().isEmpty ? 'obligatorio' : null,
                              onSaved: (String value) =>
                                  this._room.name = value.trim(),
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction),
                          SizedBox(height: 30),
                          GeneralInputField(
                              width: 150,
                              prefixIcon: Icon(Icons.person),
                              keyboardType: TextInputType.number,
                              labelText: "Cantidad Personas",
                              initialValue: this._room.capacity == 0
                                  ? null
                                  : this._room.capacity.toString(),
                              validator: (String value) {
                                if (value.trim().isEmpty) return 'obligatorio';

                                try {
                                  var capacity = int.tryParse(value);

                                  if (capacity <= 0)
                                    return 'capacidada debe ser mayor a 0';
                                  return null;
                                } catch (e) {
                                  return "Este campo debe ser un número";
                                }
                              },
                              onSaved: (String value) =>
                                  this._room.capacity = int.parse(value),
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction),
                          SizedBox(
                            height: 40,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
