import 'package:flutter/material.dart';
import 'package:kforo/models/building_model.dart';
import 'package:kforo/src/widgets/inputs/general_input.dart';

class CreateBuildForm extends StatefulWidget {
  final BuildingModel building;
  CreateBuildForm(Key key, {this.building}) : super(key: key);

  @override
  CreateBuildFormState createState() => CreateBuildFormState(this.building);
}

class CreateBuildFormState extends State<CreateBuildForm> {
  final _formKey = GlobalKey<FormState>();

  BuildingModel _building;
  bool _updateMode;

  CreateBuildFormState(this._building) {
    if (this._building == null) {
      this._building = BuildingModel(location: '', capacity: 0, name: '');
      _updateMode = false;
    } else {
      _updateMode = true;
    }
  }

  Future<bool> send() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      if (_updateMode) {
        await this._building.update();
      } else {
        await this._building.createNew();
      }

      return true;
    } else {
      return false;
    }
  }

  bool validate() {
    return _formKey.currentState.validate();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Positioned(
              child: FittedBox(
                child: Card(
                  child: Container(
                    padding: const EdgeInsets.only(
                      left: 20,
                      top: 50,
                      right: 20,
                      bottom: 10,
                    ),
                    width: 300,
                    alignment: Alignment.center,
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          GeneralInputField(
                              width: 150,
                              prefixIcon: Icon(Icons.business),
                              keyboardType: TextInputType.text,
                              labelText: "Nombre Edificio",
                              initialValue: this._building.name.isEmpty
                                  ? null
                                  : this._building.name,
                              validator: (String value) =>
                                  value.trim().isEmpty ? 'obligatorio' : null,
                              onSaved: (String value) =>
                                  this._building.name = value.trim(),
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction),
                          SizedBox(height: 30),
                          GeneralInputField(
                              width: 150,
                              prefixIcon: Icon(Icons.person),
                              keyboardType: TextInputType.number,
                              labelText: "Cantidad Personas",
                              initialValue: this._building.capacity == 0
                                  ? null
                                  : this._building.capacity.toString(),
                              validator: (String value) {
                                if (value.trim().isEmpty) return 'obligatorio';
                                try {
                                  var capacity = int.tryParse(value);

                                  if (capacity <= 0)
                                    return 'capacidad debe ser mayor a 0';
                                  return null;
                                } catch (e) {
                                  return "Este campo debe ser un número";
                                }
                              },
                              onSaved: (String value) =>
                                  this._building.capacity = int.parse(value),
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction),
                          SizedBox(
                            height: 40,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
