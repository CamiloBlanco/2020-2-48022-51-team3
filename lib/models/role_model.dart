import 'package:cloud_firestore/cloud_firestore.dart';

class RoleModel {
  static const String collectionName = 'role';

  static const String _name = 'name';
  static const String _watchmanName = 'Vigilante';
  static const String _adminName = 'Administrador';
  static const String _personName = 'Persona';

  static final CollectionReference _collectionReference =
      FirebaseFirestore.instance.collection(RoleModel.collectionName);

  DocumentReference reference;
  String name;

  RoleModel({this.name});

  RoleModel.fromMap(Map<String, dynamic> map, {this.reference})
      : assert(map[_name] != null),
        this.name = map[_name];

  RoleModel.fromSnapshot(DocumentSnapshot snap)
      : this.fromMap(snap.data(), reference: snap.reference);

  static List<RoleModel> listFromSnapshot(List<DocumentSnapshot> snaps) =>
      snaps.map((snap) => new RoleModel.fromSnapshot(snap)).toList();

  static Future<List<RoleModel>> fetchAll() async =>
      RoleModel.listFromSnapshot((await _collectionReference.get()).docs);

  get isAWatchman => this.name == _watchmanName;
  get isAdmin => this.name == _adminName;
  get isPerson => this.name == _personName;

  @override
  bool operator ==(Object obj) {
    if (obj is RoleModel) {
      final RoleModel docType = obj;
      return this.reference.id == docType.reference.id;
    } else {
      return false;
    }
  }

  @override
  int get hashCode => super.hashCode;
}
