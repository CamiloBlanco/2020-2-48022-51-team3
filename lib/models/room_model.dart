import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kforo/models/building_model.dart';

class RoomModel {
  static const String collectionName = 'room';

  static const String _name = 'name';
  static const String _capacity = 'capacity';
  static const String _location = 'location';
  static const String _building = 'building';

  static final CollectionReference _collectionReference =
      FirebaseFirestore.instance.collection(RoomModel.collectionName);

  DocumentReference reference;
  String name;
  String location;
  int capacity;

  BuildingModel building;
  DocumentReference buildingReference;

  RoomModel({this.name, this.capacity, this.location, this.buildingReference});

  RoomModel.fromMap(Map<String, dynamic> map, {this.reference})
      : assert(map[_name] != null),
        assert(map[_capacity] != null),
        assert(map[_location] != null),
        assert(map[_building] != null),
        this.name = map[_name],
        this.location = map[_location],
        this.capacity = map[_capacity],
        this.buildingReference = map[_building];

  RoomModel.fromSnapshot(DocumentSnapshot snap)
      : this.fromMap(snap.data(), reference: snap.reference);

  static List<RoomModel> listFromSnapshot(List<DocumentSnapshot> snaps,
          {bool populate: true}) =>
      populate
          ? snaps
              .map((snap) async =>
                  await (new RoomModel.fromSnapshot(snap)).populate())
              .toList()
          : snaps.map((snap) => new RoomModel.fromSnapshot(snap)).toList();

  static Future<List<RoomModel>> fetchRoomsByBuilding(
      DocumentReference buildingReference,
      {bool populate}) async {
    final collection = await _collectionReference
        .where(_building, isEqualTo: buildingReference)
        .get();

    return RoomModel.listFromSnapshot(collection.docs, populate: populate);
  }

  Map<String, dynamic> _toMap() => {
        _name: this.name,
        _location: this.location,
        _capacity: this.capacity,
        _building: this.buildingReference
      };

  Future<void> createNew() async {
    assert(this.reference == null);
    this.reference = await _collectionReference.add(this._toMap());
  }

  Future<void> update() async => await this.reference.update(this._toMap());

  Future<RoomModel> populate() async {
    this.building =
        BuildingModel.fromSnapshot(await this.buildingReference.get());
    return this;
  }
}
