import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kforo/models/room_model.dart';

class BuildingModel {
  static const String collectionName = 'building';

  static const String _name = 'name';
  static const String _capacity = 'capacity';
  static const String _location = 'location';

  static final CollectionReference collectionReference =
      FirebaseFirestore.instance.collection(BuildingModel.collectionName);

  List<RoomModel> _rooms;

  DocumentReference reference;
  String name;
  String location;
  int capacity;

  BuildingModel({this.name, this.capacity, this.location});

  BuildingModel.fromMap(Map<String, dynamic> map, {this.reference})
      : assert(map[_name] != null),
        assert(map[_capacity] != null),
        assert(map[_location] != null),
        this.name = map[_name],
        this.location = map[_location],
        this.capacity = map[_capacity];

  BuildingModel.fromSnapshot(DocumentSnapshot snap)
      : this.fromMap(snap.data(), reference: snap.reference);

  static List<BuildingModel> listFromSnapshot(List<DocumentSnapshot> snaps) =>
      snaps.map((snap) => (new BuildingModel.fromSnapshot(snap))).toList();

  Map<String, dynamic> _toMap() =>
      {_name: this.name, _location: this.location, _capacity: this.capacity};

  Future<List<RoomModel>> rooms() async {
    if (_rooms == null) {
      await this.fetchRooms();
    }
    return _rooms;
  }

  Future<void> createNew() async {
    assert(this.reference == null);
    this.reference = await collectionReference.add(this._toMap());
  }

  Future<void> update() async => await this.reference.update(this._toMap());

  Future<void> fetchRooms() async {
    this._rooms =
        await RoomModel.fetchRoomsByBuilding(this.reference, populate: false);
  }
}
