import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:kforo/models/auth_module.dart';
import 'package:kforo/models/document_type_model.dart';
import 'package:kforo/models/role_model.dart';

class UserModel {
  static const String collectionName = 'user';

  static const String _name = 'name';
  static const String _role = 'role';
  static const String _documentType = 'document_type';
  static const String _email = 'email';
  static const String _document = 'document';
  static const String _imgPath = 'imgPath';
  static const String _authId = 'authId';

  static final CollectionReference collectionReference =
      FirebaseFirestore.instance.collection(UserModel.collectionName);

  static UserModel currentUser;

  String name;
  String document;
  String imgPath;
  String email;

  DocumentReference roleReference;
  RoleModel role;

  DocumentReference documentTypeReference;
  DocumentTypeModel documentType;

  DocumentReference reference;

  UserModel({this.name, this.imgPath, this.reference, this.roleReference});

  UserModel.fromMap(Map<String, dynamic> map, {this.reference, User userAuth})
      : assert(map[_name] != null),
        assert(map[_role] != null),
        assert(map[_email] != null),
        assert(map[_document] != null),
        assert(map[_documentType] != null),
        name = map[_name],
        email = map[_email],
        document = map[_document],
        documentTypeReference = map[_documentType],
        roleReference = map[_role],
        imgPath = map[_imgPath] != null
            ? map[_imgPath]
            : "https://jshopping.in/images/detailed/591/ibboll-Fashion-Mens-Optical-Glasses-Frames-Classic-Square-Wrap-Frame-Luxury-Brand-Men-Clear-Eyeglasses-Frame.jpg";

  UserModel.fromSnapshot(DocumentSnapshot snapshot, {User userAuth})
      : this.fromMap(snapshot.data(),
            reference: snapshot.reference, userAuth: userAuth);

  static Future<UserModel> signin(String email, String password) async {
    await (new Auth()).signIn(email, password);

    User userAuth = FirebaseAuth.instance.currentUser;

    final query = await FirebaseFirestore.instance
        .collection(UserModel.collectionName)
        .where(_authId, isEqualTo: userAuth.uid)
        .get();

    final user =
        await UserModel.fromSnapshot(query.docs.first, userAuth: userAuth)
            .populate();

    UserModel.currentUser = user;

    return user;
  }

  static Stream<QuerySnapshot> snapshot() => FirebaseFirestore.instance
      .collection(UserModel.collectionName)
      .snapshots();

  static Future<List<UserModel>> listFromSnapshot(
          List<DocumentSnapshot> snapshots) async =>
      await Future.wait(snapshots
          .map((snap) async =>
              (await (new UserModel.fromSnapshot(snap)).populate()))
          .toList());

  Future<UserModel> populate() async {
    this.role = RoleModel.fromSnapshot(await this.roleReference.get());
    this.documentType =
        DocumentTypeModel.fromSnapshot(await this.documentTypeReference.get());
    return this;
  }

  Future<void> signOut() async {
    await (new Auth()).signOut();
    UserModel.currentUser = null;
  }

  static Future<void> signUp(
      {String name,
      String email,
      String password,
      String document,
      DocumentReference role,
      DocumentReference documentType}) async {
    String uid = await (new Auth()).signUp(email, password);

    Map<String, dynamic> userData = {
      _authId: uid,
      _name: name,
      _email: email,
      _role: role,
      _document: document,
      _documentType: documentType
    };
    await UserModel.collectionReference.add(userData);
  }

  Future<void> update(
      {String name,
      String document,
      RoleModel role,
      DocumentTypeModel documentType}) async {
    this.name = name;
    this.document = document;
    this.role = role;
    this.documentType = documentType;
    this.roleReference = role.reference;
    this.documentTypeReference = documentType.reference;

    Map<String, dynamic> userData = {
      _name: name,
      _role: this.roleReference,
      _document: document,
      _documentType: this.documentTypeReference
    };
    await this.reference.update(userData);
  }
}
