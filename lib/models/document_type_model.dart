import 'package:cloud_firestore/cloud_firestore.dart';

class DocumentTypeModel {
  static const String collectionName = 'document_type';

  static const String _name = 'name';
  static const String _validationRegex = 'validation_regex';
  static const String _errorMsg = 'error_message';

  static final CollectionReference collectionReference =
      FirebaseFirestore.instance.collection(DocumentTypeModel.collectionName);

  final DocumentReference reference;
  final String name;
  final String validationRegex;
  final String errorMsg;

  DocumentTypeModel(
      {this.reference, this.name, this.validationRegex, this.errorMsg});

  DocumentTypeModel.fromMap(Map<String, dynamic> map, this.reference)
      : assert(map[_name] != null),
        assert(map[_validationRegex] != null),
        assert(map[_errorMsg] != null),
        this.name = map[_name],
        this.validationRegex = map[_validationRegex],
        this.errorMsg = map[_errorMsg];

  DocumentTypeModel.fromSnapshot(DocumentSnapshot snap)
      : this.fromMap(snap.data(), snap.reference);

  static List<DocumentTypeModel> listFromSnapshot(
          List<DocumentSnapshot> snaps) =>
      snaps.map((snap) => new DocumentTypeModel.fromSnapshot(snap)).toList();

  static Future<List<DocumentTypeModel>> fetchAll() async =>
      DocumentTypeModel.listFromSnapshot(
          (await DocumentTypeModel.collectionReference.get()).docs);

  @override
  bool operator ==(Object obj) {
    if (obj is DocumentTypeModel) {
      final DocumentTypeModel docType = obj;
      return this.reference.id == docType.reference.id;
    } else {
      return false;
    }
  }

  @override
  int get hashCode => super.hashCode;
}
