# kforo

A new Flutter project. 

## Getting Started

This project is a starting point for a Flutter application for Ing Software II.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.

## Pull request steps

To make a pull request for Dev you first must follow this steps:

1. Commit all your changes. Verify there are no un commited changes.

    * If you have some changes that you don't want to commit (for example, a piece of code in the login to avoid login for development) make a stash.
        ```bash
          $ git stash
        ```

1. Change to Dev branch and update it.
    ```bash
       $ git checkout Dev
       $ git pull
    ```

1. Change to `your-branch` and merge Dev to it
    ```bash
      $ git checkout <your-branch>
      $ git merge Dev
    ```

1. Verify and fix confilcts. This is done in the code (vs code recommended for this task)

1. Check pull-reuest requirements. Run the following commands in root of the project
    ```bash
      $ flutter test
      $ flutter analyze
    ```

    1. If `flutter test` fails verify the code that is failing and fix it (or contact to the author of that code).
    1. If `flutter analyze` fails verify the code that is failing and fix it. If it fails with an **error** flag you must fix this!. If it fails with an **info** flag and you cannot avoid or fix it this can be discussed with the team to check if it can be avoided.

1. Push your branch to the repo
    ```bash
      $ git push
    ```

    * If you have made a `git stash` remember to pop this stash.
      ```bash
        $ git stash pop
      ```

1. In bibucket create a pull request!